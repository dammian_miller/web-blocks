// import _ from 'lodash/core';
import jQuery from 'jquery'; // eslint-disable-line no-unused-vars
import mockjax from 'jquery-mockjax'; // eslint-disable-line no-unused-vars
import WebBlocks from './lib/WebBlocks/WebBlocks';
import WBTemplate from './lib/WebBlocks/WBTemplate'; // eslint-disable-line no-unused-vars
import docReady from './lib/docReady';
// import ui from './lib/ui';

docReady(() => {
	console.log('Doc Ready!');
	ui.init();
});

let $;
let jquery;
$ = jquery = require('jquery'); // eslint-disable-line no-multi-assign, prefer-const
var _ = require('lodash/core');
window._ = _;

window.jQuery = $;
window.$ = $;

window.mockjax = require('jquery-mockjax')(jquery, window);

WebBlocks.bind('template');
