const WebBlocks = (((debug) => {
	debug = true;

	const blocksRegistry = {};

	const registerBlock = (blockSelector, handler) => {
		if (debug) {
			console.log(`WB registering block: ${blockSelector.replace('data-', '')}`);
		}
		blocksRegistry[blockSelector.replace('data-', '')] = handler;
	};

	const bindBlocks = (blockName, rootSelector) => {
		let blockHandler;
		if (typeof rootSelector === 'undefined') {
			rootSelector = document;
		}
		if (blockName === undefined) {
			for (const block in blocksRegistry) {
				if (debug) {
					console.log(`WB binding block: ${block}`);
				}
				blockHandler = blocksRegistry[block];
				// console.log( 'blockHandler: ' + blockHandler );
				$(rootSelector).find(`[data-${block}]`).each(blockHandler);
			}
		} else {
			if (debug) {
				console.log(`WB binding block: ${blockName}`);
			}
			blockHandler = blocksRegistry[blockName];
			// console.log( 'blockHandler: ' + blockHandler );
			$(rootSelector).find(`[data-${blockName}]`).each(blockHandler);
		}
	};
	return {register: registerBlock, bind: bindBlocks};
})());
export default WebBlocks;
