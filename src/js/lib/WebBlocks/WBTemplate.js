import WebBlocks from './WebBlocks';
var _ = require('lodash');
window._ = _;

const WBTemplate = ((() => {
	WebBlocks.register(
	// WebBlock name with the 'data-' omitted
	'template',
	// framework-agnostic function which binds to HTML microformat data-attributes
	function wbTemplate() {
		console.time( 'template' );
		const tgtElm = this;
		const tmplDataSrc = $(this).data('templateDataSrc');
		const tmplSelector = $(this).data('template');
		const tmplContent =  $(`#${tmplSelector}`).html().trim();
		let tmplContentClone = $(tmplContent);
		const bindTemplateData = (template, data) => {
		 $(template)
				.find('[data-template-map-directive]')
				.each(function(count){
					if($(this).attr('data-template-map-directive-processed') !== undefined)
						return;
					const mapDir = $(this).data('templateMapDirective');
					const mapVal = _.get(data, mapDir);
					if(typeof mapVal !== 'undefined'){
						if(typeof mapVal !== 'object'){
							const directiveTarget = $(this).data('templateMapTargetAttribute');
							if(directiveTarget !== undefined)
							{
									$(this).attr(directiveTarget,mapVal);
							}	else {
								$(this).html(mapVal);
							}
							$(this)
								.removeAttr('data-template-map-directive-processed')
								.removeAttr('data-template-map-target-attribute')
								.removeAttr('data-template-map-directive');
						}
						else {
							let arrResults = [];
							$(this).attr('data-template-map-directive-processed', 'true');
							for(let i = 0, l = mapVal.length; i < l; i++){
								const fragment = $(this).clone(true)
									.removeAttr('data-template-map-directive-processed')
									.removeAttr('data-template-map-target-attribute')
									.removeAttr('data-template-map-directive');
								arrResults.push(bindTemplateData( fragment, mapVal[i]));
							}
							$(this).replaceWith(arrResults);
						}
					}
				});
			return template;
		}

		$(document).ready(() => {
			$.ajax({
				type: 'POST',
				url: tmplDataSrc,
				data: {
					Data: 'JAAxOxAhi6lzo04xQlh8+caFQ8Ihws4UZaxhiNJulGAUzpRVgjjJCy9z2trC8kn+qId8v6lnQXGnBTD7qSydflulQw/FSx7AHEzIpiiVyMc3+/soF5W/cVDkAZD75iy4i1teKwP8Gtqp6VnvAkgz2zWC0LHSL6u36ziB9x18yb81HtthZoyZYr40+t/qUwoXJbNZGY4ZSlNFcVv9NCR9ZA==',
					Initialisation: '6Itl/1VqkYh3MX1V0bPoAQ==',
					Key: 'UbjtGQBuTk821hg/L+ryfE1YNo/i9s0JtpGrL86Dmgsc++hY1QRQemxpvE5g2r1290Mr7fesiAEN2wbvcAcsbw=="}',
					receiptCode: '7f65ccd6e29f'
				},
				beforeSend() {
					// this is where we append a loading image
				},
				success(data) { // eslint-disable-line no-unused-vars
					$(tgtElm).append(
						$(bindTemplateData(tmplContentClone,data))
					);
					console.timeEnd( 'template' );
				},
				error() {
					// failed request; give feedback to user
				}
			});
		});
	});
}))();

export default WBTemplate;
