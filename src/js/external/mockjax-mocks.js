window.mockjax({
	    url: '/receipt/retrieve',
	    // Use a random value between 250ms and 750ms
	    responseTime: [250, 750],
	    responseText: {
	     Result: {
	      LineItems: [{
	        ProductId: 844142,
	        LineItemId: 1,
	        Url: '/316771',
	        DisplayName: 'Soniq S55UV16A-AU 55\" 4K UHD Smart LED LCD TV',
	        Image: {
	         AltDescription: 'Soniq S55UV16A-AU 55\" 4K UHD Smart LED LCD TV',
	         Uri: '/FileLibrary/ProductResources/Images/189450-T-LO.jpg'
	        },
	        Quantity: 1,
	        Total: 599.0
	       },
	       {
	        ProductId: 842748,
	        LineItemId: 2,
	        Url: '/312411',
	        DisplayName: 'HP 15-BA038AU 15\" Laptop',
	        Image: {
	         AltDescription: 'HP 15-BA038AU 15\" Laptop',
	         Uri: '/FileLibrary/ProductResources/Images/192776-T-LO.jpg'
	        },
	        Quantity: 1,
	        Total: 548.0
	       }
	      ],
	      PurchaseType: 'online',
	      PurchaseDate: '06/01/2017 19:31:58',
	      HasDeliveryItem: true,
	      ItemsCount: 2,
	      ShippingTotal: 49.0,
	      SubTotal: 1147.0,
	      Total: 1196.0,
	      TotalGST: 108.73
	     },
	     Status: {
	      ActionType: 0,
	      IsSuccess: true,
	      ActionString: '<p class="error"><strong>Oops!</strong> Try that again in a few moments.</p>'
	     }
	    }
	   });
