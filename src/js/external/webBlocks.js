/**********************************************
 ********** 3rd-Party Dependencies *************
 **********************************************/
// export default function webBlocks(callback) {

var debug = true;

// var $ = require('jquery');
window.jQuery = $;
window.$ = $;

var alertFallback = false;
if ( typeof console === "undefined" || typeof console.log === "undefined" ) {
	console = {};
	if ( alertFallback ) {
		console.log = function ( msg ) {
			alert( msg );
		};
		console.debug = function ( msg ) {
			alert( msg );
		};
		console.info = function ( msg ) {
			alert( msg );
		};
		console.warn = function ( msg ) {
			alert( msg );
		};
		console.error = function ( msg ) {
			alert( msg );
		};
	} else {
		console.log = function () {};
		console.debug = function () {};
		console.info = function () {};
		console.warn = function () {};
		console.error = function () {};
	}
}

if ( typeof console !== undefined && console.groupCollapsed === undefined ) {
	console.groupCollapsed = function () {};
	console.groupEnd = function () {};
}

// console.time implementation for IE
if ( typeof console !== undefined && console.time === undefined ) {
	console.time = function ( name, reset ) {
		if ( !name ) {
			return;
		}
		var time = new Date()
			.getTime();
		if ( !console.timeCounters ) {
			console.timeCounters = {};
		}
		var key = "KEY" + name.toString();
		if ( !reset && console.timeCounters[ key ] ) {
			return;
		}
		console.timeCounters[ key ] = time;
	};

	console.timeEnd = function ( name ) {
		var time = new Date()
			.getTime();
		if ( !console.timeCounters ) {
			return;
		}
		var key = "KEY" + name.toString();
		var timeCounter = console.timeCounters[ key ];
		var diff;
		if ( timeCounter ) {
			diff = time - timeCounter;
			var label = name + ": " + diff + "ms";
			console.info( label );
			delete console.timeCounters[ key ];
		}
		return diff;
	};
}

function getArgsArray( args ) {
	if ( args !== undefined && args !== null )
		return Array.prototype.slice.call( args );
	else
		return [];
}

jQuery.events = function(expr) {
    var rez = [],
        evo;
    jQuery(expr).each(
        function() {
            if (evo = jQuery._data(this, "events"))
                rez.push({
                    element: this,
                    events: evo
                });
        }
    );
    return rez.length > 0 ? rez : null;
}

jQuery.fn.outerHTML = function() {
    return jQuery("<div />").append(this.eq(0).clone()).html();
};

// continue event handlers from position of handler namespace
jQuery.event.triggerContinue = function(event, data, elem, startHandlerNamespace) {
    try {
        var events = $.events(elem)[0]["events"][event.type];
        // console.log(events);
        var evt = undefined;
        var indexFound = false;
        for (var i = 0, il = events.length; i < il; i++) {
            evt = events[i];
            console.log(evt);
            if (evt.namespace === startHandlerNamespace) {
                indexFound = true;
                continue;
            }
            if (indexFound) {
                evt.handler.run(elem, event);
            }
        }
    } catch (err) {
        console.warn(err);
    }
}

// assign original jQuery.event.trigger to value
var oldTriggerFn = jQuery.event.trigger;
// // override jQuery.event.trigger with proxy that performs additional checks then conditionally calls the original function on pass of conditions.
jQuery.event.trigger = function(event, data, elem, onlyHandlers) {
    // Our custom val function calls the original val and then triggers the change event.
    var triggerResponse,
        type = event.type || event,
        namespaces = [],
        cache, exclusive, i, cur, old, ontype, special, handle, eventPath, bubbleType;
    if (type.indexOf("!") >= 0) {
        // Exclusive events trigger only for the exact event (no namespaces)
        type = type.slice(0, -1);
        exclusive = true;
    }
    if (type.indexOf(".") >= 0) {
        // Namespaced trigger; create a regexp to match event type in handle()
        namespaces = type.split(".");
        type = namespaces.shift();
        namespaces.sort();
    }
    event = typeof event === "object" ?
        // jQuery.Event object
        event[jQuery.expando] ? event :
        // Object literal
        new jQuery.Event(type, event) :
        // Just the event type (string)
        new jQuery.Event(type);
    // if( debug )
    // 	console.log( "event.override_event_flood_block: " + event.override_event_flood_block );
    if (
        elem === (document) ||
        event.type !== "click" ||
        $(elem).attr("data-" + event.type + "-handler-active") !== "true" ||
        event.override_event_flood_block
    ) {
        $(elem).attr("data-" + event.type + "-handler-active", "true");
        // if( debug )
        // 	console.log( elem );
        var args = getArgsArray(arguments);
        // args[0].override_event_flood_block = true;
        triggerResponse = oldTriggerFn.apply(this, args);
        setTimeout(function() {
            $(elem).attr("data-" + event.type + "-handler-active", "");
            $(elem).removeAttr("data-" + event.type + "-handler-active");
            // if( debug )
            // {
            //   		console.log( "handler state cleared on timeout" );
            //   		// console.log( elem );
            // }
        }, 500);
    } else {
        if (debug) {
            console.log("event blocked");
            // console.log( event );
        }
    }
    return triggerResponse;
}

// save reference to original jQuery.event.dispatch
var oldDispatchFn = jQuery.event.dispatch;
// // override jQuery.event.dispatch with proxy that performs additional checks then conditionally calls the original function on pass of conditions.
jQuery.event.dispatch = function(event) {
    // Our custom val function calls the original val and then triggers the change event.
    var dispatchResponse;
    // if( debug )
    // 	console.log( "event.override_event_flood_block: " + event.override_event_flood_block );
    if (
        this === (document) ||
        event.type !== "click" ||
        $(this).attr("data-" + event.type + "-handler-active") !== "true" ||
        event.override_event_flood_block
    ) {
        $(this).attr("data-" + event.type + "-handler-active", "true");
        dispatchResponse = oldDispatchFn.apply(this, arguments);
        (function(elem, event) {
            setTimeout(function() {
                try {
                    $(elem).attr("data-" + event.type + "-handler-active", "");
                    $(elem).removeAttr("data-" + event.type + "-handler-active");
                    // if( debug )
                    // {
                    //   		console.log( "handler state cleared on timeout" );
                    // }
                } catch (err) {}
            }, 500);
        })(this, event);
    } else {
        if (debug) {
            console.log("event blocked");
            // 	console.log( event );
        }
    }
    return dispatchResponse;
}


// Thanks to bvukelic @ http://stackoverflow.com/a/9354412
var __scrollTimer;
$( window )
	.on( "scroll",
		function ( e ) {
			if ( __scrollTimer ) {
				clearTimeout( __scrollTimer );
			}
			__scrollTimer = setTimeout(
				function () {
					if ( debug )
						console.log( "scroll-viewport triggered" );
					var scrollEvent = new $.Event( "scroll-viewport", {
						override_event_flood_block: true
					} );
					scrollEvent.override_event_flood_block = true;
					$( window )
						.trigger( scrollEvent );
				},
				1
			);
		}
	);

// Thanks to:
// http://www.abeautifulsite.net/blog/2011/11/detecting-mobile-devices-with-javascript/
var isMobile = {
	Android: function () {
		return navigator.userAgent.match( /Android/i );
	},
	BlackBerry: function () {
		return navigator.userAgent.match( /BlackBerry/i );
	},
	iOS: function () {
		return navigator.userAgent.match( /iPhone|iPad|iPod/i );
	},
	Opera: function () {
		return navigator.userAgent.match( /Opera Mini/i );
	},
	Windows: function () {
		return navigator.userAgent.match( /IEMobile/i );
	},
	any: function () {
		return ( isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() );
	}
};

var jumpTo = function ( selector ) {
	var p = $( selector )
		.position();
	window.scrollTo( p.left, p.top );
}

var spotlightUsername = function () {
	var tgtField = $( '#username' );
	var faketgtField = $( '#fakeusername' );

	jumpTo( faketgtField );

	if ( faketgtField.css( 'display' ) !== 'none' ) {
		faketgtField.fadeOut( 500, function () {
				tgtField.hide();
			} )
			.fadeIn( 500, function () {
				faketgtField.focus();
			} );
	} else {
		tgtField.focus();
	}
}

// default Array.indexOf does not honor original cast type
Array.prototype.indexOf = function ( elt /*, from*/ ) {
	var len = this.length >>> 0;
	var from = Number( arguments[ 1 ] ) || 0;
	from = ( from < 0 ) ?
		Math.ceil( from ) :
		Math.floor( from );
	if ( from < 0 )
		from += len;

	for ( ; from < len; from++ ) {
		if ( from in this &&
			this[ from ] === elt || this[ from ].toString() == elt )
			return from;
	}
	return -1;
};

/*
 IntegraXor Web SCADA - JavaScript Number Formatter
 http://www.integraxor.com/
 author: KPL, KHL
 (c)2011 ecava
 Dual licensed under the MIT or GPL Version 2 licenses.
*/
window.format = function ( b, a ) {
	if ( !b || isNaN( +a ) ) return a;
	var a = b.charAt( 0 ) == "-" ? -a : +a,
		j = a < 0 ? a = -a : 0,
		e = b.match( /[^\d\-\+#]/g ),
		h = e && e[ e.length - 1 ] || ".",
		e = e && e[ 1 ] && e[ 0 ] || ",",
		b = b.split( h ),
		a = a.toFixed( b[ 1 ] && b[ 1 ].length ),
		a = +a + "",
		d = b[ 1 ] && b[ 1 ].lastIndexOf( "0" ),
		c = a.split( "." );
	if ( !c[ 1 ] || c[ 1 ] && c[ 1 ].length <= d ) a = ( +a )
		.toFixed( d + 1 );
	d = b[ 0 ].split( e );
	b[ 0 ] = d.join( "" );
	var f = b[ 0 ] && b[ 0 ].indexOf( "0" );
	if ( f > -1 )
		for ( ; c[ 0 ].length < b[ 0 ].length - f; ) c[ 0 ] = "0" + c[ 0 ];
	else +c[ 0 ] == 0 && ( c[ 0 ] = "" );
	a = a.split( "." );
	a[ 0 ] = c[ 0 ];
	if ( c = d[ 1 ] && d[ d.length -
			1 ].length ) {
		for ( var d = a[ 0 ], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++ ) f += d.charAt( g ), !( ( g - k + 1 ) % c ) && g < i - c && ( f += e );
		a[ 0 ] = f
	}
	a[ 1 ] = b[ 1 ] && a[ 1 ] ? h + a[ 1 ] : "";
	return ( j ? "-" : "" ) + a[ 0 ] + a[ 1 ]
};

// automagically determine whether simple selector or script-based selector - must use jQuery
function resolveSelector( sel ) {
	var bindElm;
	if ( debug )
		console.log( "selector type: " + ( typeof sel ) );
	if ( debug )
		console.log( "selector: " + sel );

	if ( sel !== undefined && typeof sel === "string" ) {
		sel = sel.toString();
		if ( sel.indexOf( "$(" ) > -1 ) {
			bindElm = new Function(
					"return " + sel + ";"
				)
				.run( this );
		} else {
			bindElm = new Function(
					"return $( \"" + sel + "\" );"
				)
				.run( this );
		}
	} else if ( sel !== undefined && typeof sel === "object" ) {
		if ( debug )
			console.warn( "resolveSelector: object detected.  returning" );
		return sel;
	} else {
		if ( debug )
			console.warn( "resolveSelector: No parameter supplied" );
	}
	if ( debug )
		console.log( bindElm );
	return bindElm;
}

function execStringWithScope( runCode, root, delay ) {
	if ( runCode !== undefined && root !== undefined && runCode.length > 0 ) {
		if ( delay === undefined )
			delay = 50;
		if ( debug )
			console.log( "runCode: " + runCode );
		setTimeout( new Function( runCode.toString() )
			.call( root ), delay );
		// new Function( return runCode.toString() ).call( root );
	}
}

/**********************************************
 ********** Progressive Behaviours *************
 **********************************************/

/*****************************************
 ***** Element Content via Forumla w/ *****
 *** Multi-Element source value support ***
 *****************************************/
// This widget provides dynamic element content
// via a formula which can use values sourced from
// other elements
// Apply the following data attributes to an element to
// utilise this widget:
// 1. data-content-from-selectors-formula
//  *required
//  use selectors and operators to calculate a final value
//  selector content must be numeric or a NaN will be returned

function bindValueFromFormula( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var parts = $( this )
					.attr( "data-value-from-formula" )
					.split( " " ),
					formula = "",
					targetElm = this,
					fmt = $( this )
					.attr( "data-numeric-display-format" );
				if ( fmt === undefined ) {
					fmt = "#,##0.00";
				}
				for ( var i = 0, l = parts.length; i < l; i++ ) {
					parts[ i ] = parts[ i ].trim();
					switch ( parts[ i ] ) {
						case "+":
						case "-":
						case "*":
						case "/":
						case "%":
						case "(":
						case ")":
						case "=":
						case "==":
						case "===":
						case "&&":
						case "!":
						case "^":
							formula += parts[ i ];
							break;
						default:
							var elm = $( parts[ i ] );
							if ( elm.is( "input" ) ) {
								elm.on(
									'input blur',
									function ( e ) {
										e.preventDefault();
										$( targetElm )
											.trigger( "watchedchange" );
									}
								);
							}
							if ( elm.is( "input" ) ) {
								formula += "parseFloat($('" + parts[ i ] + "').val(),10)";
							} else {
								formula += "parseFloat($('" + parts[ i ] + "').html(),10)";
							}
							break;
					}
				}
				$( this )
					.off(
						"watchedchange.value-from-formula"
					)
					.on(
						"watchedchange.value-from-formula",
						function () {
							$( this )
								.html( format( fmt, eval( formula ) ) );
						}
					)
					.trigger( "watchedchange" );
			}
		);
}

/*****************************************
 ** Link Values - Slider<->Input Widget ***
 *****************************************/
// This widget is a progressive enhancement
// which knows about a specific microformat
// implemented using the HTML5 data-attribute
// This widget enables linking values between multiple
// elements.
// eg. <input type="range" />  <->  <input type="number" />
// Apply the following data attributes to an element to
// utilise this widget:
// 1. data-link-value-target-selectors
//  *required
//  selector to match element to set value on

function bindLinkValues( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var targetIds = "",
					targetElements = [];
				if ( $( this )
					.attr( "data-link-value-target-selectors" ) !== undefined )
					targetIds = $( this )
					.attr( "data-link-value-target-selectors" )
					.split( "," );
				for ( var i = 0, l = targetIds.length; i < l; i++ ) {
					if ( $( targetIds[ i ] ) != "undefined" ) {
						targetElements.push( $( targetIds[ i ] ) );
					}
				}
				$( this )
					.off(
						'change.link-values'
					)
					.data(
						"link-value-target-elements",
						targetElements
					)
					.on(
						'change.link-values',
						function ( e ) {
							e.preventDefault();
							for ( var i = 0, l = targetElements.length; i < l; i++ ) {
								targetElements[ i ].val( $( this )
									.val() );
								targetElements[ i ].trigger( "input" );
							}
						}
					);
			}
		);
}

/*****************************************
 **** Accordion Expand/Collapse Widget ****
 *****************************************/
// This widget is a progressive enhancement
// which knows about a specific microformat
// implemented using the HTML5 data-attribute
// This widget enables expand/collapse function
// when applied to an element that sits within
// a panel with classes panel__header, panel__body, etc
// Insert the following HTML structure inside a panel to
// utilise this widget:
// <div data-accordion>
// 	 <span>&nbsp;</span>
// </div>
// Note: this widget is only activated when it is visible
// To disable simply set display to none
// This allows for the same panel to have different behaviour
// depending on location


// create new with target
function bindAccordionBehaviour( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var prnt = $( this )
					.closest( '.panel__header' ),
					root = $( "body" ),
					minimize = false,
					onexpand = "",
					onexpandEventDelay = 0,
					oncollapse = "",
					oncollapseEventDelay = 0,
					saveState = true;
				if ( prnt.hasClass( 'panel__header' ) )
					prnt = $( prnt )
					.parent();
				if ( $( this )
					.attr( "data-accordion-onexpand" ) !== undefined ) {
					onexpand = $( this )
						.attr( "data-accordion-onexpand" );
				}
				if ( $( this )
					.attr( "data-accordion-onexpand-event-delay" ) !== undefined ) {
					onexpandEventDelay = $( this )
						.attr( "data-accordion-onexpand-event-delay" );
				}
				if ( $( this )
					.attr( "data-accordion-oncollapse" ) !== undefined ) {
					oncollapse = $( this )
						.attr( "data-accordion-oncollapse" );
				}
				if ( $( this )
					.attr( "data-accordion-oncollapse-event-delay" ) !== undefined ) {
					oncollapseEventDelay = $( this )
						.attr( "data-accordion-oncollapse-event-delay" );
				}
				if ( $( this )
					.attr( "data-accordion-save-state" ) !== undefined ) {
					saveState = $( this )
						.attr( "data-accordion-save-state" ) === "false" ? false : true;
				}
				if ( $( prnt )
					.attr( "class" ) === undefined ) {
					if ( debug )
						console.warn( "data-accordion missing classname identifying parent panel" );
					saveState = false;
				}
				if ( $( this )
					.css( "display" ) !== "none" ) {

					if ( saveState ) {
						if ( localStorage.getItem( "accordion-" + $( prnt )
								.attr( "class" )
								.split( " " )[ 0 ] ) === "true" ) {
							if ( debug )
								console.log( "loading saved collapse state" );
							$( this )
								.attr( "data-accordion-state", "collapse" );
						} else if ( localStorage.getItem( "accordion-" + $( prnt )
								.attr( "class" )
								.split( " " )[ 0 ] ) === "false" ) {
							if ( debug )
								console.log( "loading saved expand state" );
							$( this )
								.attr( "data-accordion-state", "expand" );
						}
					}

					$( this )
						.off(
							'click.accordion accordion-action.accordion'
						)
						.on(
							'click.accordion accordion-action.accordion',
							function ( e ) {
								var args = getArgsArray( arguments );
								// console.log( args );
								if ( e.accordion_processed !== undefined ) {
									e.stopPropagation();
									return;
								}
								e.accordion_processed = true;
								if ( args.length === 1 && $( this )
									.attr( "data-accordion-state" ) !== "expand" ) {
									e.stopPropagation();
									$( document )
										.trigger( "click.select-widget-blur" );
								}
								// if( debug )
								// console.log( $( this ).attr( "data-accordion-state" ) )
								if ( $( this )
									.attr( "data-accordion-onexpand" ) !== undefined ) {
									onexpand = $( this )
										.attr( "data-accordion-onexpand" );
								}
								var container = $( this )
									.closest( '.panel__header' )
									.parent();
								if ( debug ) {
									console.log( "data-accordion arguments: " + args );
								}
								var minimize = false;
								if ( args.indexOf( "init" ) > -1 && $( this )
									.attr( "data-accordion-state" ) !== undefined ) {
									if ( $( this )
										.attr( "data-accordion-state" ) === "collapse" ) {
										minimize = true;
									} else if ( $( this )
										.attr( "data-accordion-state" ) === "expand" ) {
										minimize = false;
									}
								} else if ( args.indexOf( "true" ) > -1 ) {
									minimize = true;
								} else if ( args.indexOf( "false" ) > -1 ) {
									minimize = false;
								} else {
									if ( $( this )
										.attr( "data-accordion-state" ) === "collapse" ) {
										minimize = false;
									} else if ( $( this )
										.attr( "data-accordion-state" ) === "expand" ) {
										minimize = true;
									}
								}
								if ( minimize === false ) {
									$( this )
										.attr( "data-accordion-state", "expand" );
									// $( container )
										// .removeClass( "accordion-collapse" );
									// $( container )
										// .addClass( "accordion-expand" );
									if ( saveState && arguments.length === 1 && $( container )
										.attr( "class" ) !== undefined )
										localStorage.setItem( "accordion-" + $( container )
											.attr( "class" )
											.split( " " )[ 0 ], minimize.toString() );
									if ( debug )
										console.log( "onexpand.length: " + onexpand.length );
									if ( onexpand.length > 0 ) {
										execStringWithScope.run( this, onexpand, this, onexpandEventDelay );
									}
								} else {
									$( this )
										.attr( "data-accordion-state", "collapse" );
// refactor class use accordion-expand to instead use [data-accordion-state='open']{display: block; etc.}
// [data-accordion-state='close']{styles...}
         // $( container )
										// .removeClass( "accordion-expand" );
									// $( container )
										// .addClass( "accordion-collapse" );
									if ( saveState && arguments.length === 1 && $( container )
										.attr( "class" ) !== undefined )
										localStorage.setItem( "accordion-" + $( container )
											.attr( "class" )
											.split( " " )[ 0 ], minimize.toString() );
									if ( oncollapse.length > 0 ) {
										execStringWithScope.run( this, oncollapse, this, oncollapseEventDelay );
									}
								}
							}
						);
				}
			}
		)
		.trigger( "accordion-action.accordion", "init" );
}

/***********************************************
 **** Accordion GROUP Expand/Collapse Widget ****
 ***********************************************/
// This widget is a progressive enhancement
// which knows about a specific microformat
// implemented using the HTML5 data-attribute
// This widget enables expand/collapse function
// of all child accordions from a given start element
// data attributes to utilize this widget are:
// 1. data-accordion-group-toggle="expand|collapse(optional value)"
// 2. data-accordion-group-root-element-selector="css selector or jquery function"
//		- optional - defaults to element where group toggle attribute is applied
// 3. data-accordion-group-toggle-event="eventType" - optional - defaults to "click"
function bindAccordionGroupBehaviour( selector, groupRootElementSelector, eventTypeSelector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var startElement = this;
				if ( $( this )
					.data( groupRootElementSelector.replace( "[data-", "" )
						.replace( "]", "" ) ) !== undefined ) {
					startElement = resolveSelector.run( this, $( this )
						.data( groupRootElementSelector.replace( "[data-", "" )
							.replace( "]", "" ) )
						.toString() );
				}
				var eventType = "click";
				if ( $( this )
					.attr( "data-accordion-group-toggle-event" ) !== undefined ) {
					eventType = $( this )
						.attr( "data-accordion-group-toggle-event" );
				}

				$( this )
					.off(
						eventType + ".accordionGroupBehaviour"
					)
					.on(
						eventType + ".accordionGroupBehaviour",
						function ( e ) {
							var accordions = $( startElement )
								.find( "[data-accordion]" );
							var minimize;
							// console.log( $( this ).attr( "data-" + selector.replace( "[data-", "" ).replace( "]", "" ) ) );
							if ( $( this )
								.attr( "data-" + selector.replace( "[data-", "" )
									.replace( "]", "" ) )
								.length ) {
								minimize = $( this )
									.attr( "data-" + selector.replace( "[data-", "" )
										.replace( "]", "" ) ) === "expand" ? "false" : "true";
							} else if ( $( this )
								.attr( "data-group-toggle-state" ) !== undefined ) {
								minimize = $( this )
									.attr( "data-group-toggle-state" ) === "expand" ? "true" : "false";
							} else {
								minimize = $( accordions[ 0 ] )
									.attr( "data-accordion-state" ) === "expand" ? "true" : "false";
							}
							if ( debug )
								console.log( "minimize group: " + minimize );
							accordions.each(
								function () {
									var groupAccordionEvent = new $.Event( "accordion-action.accordion", {
										override_event_flood_block: true
									} );
									groupAccordionEvent.override_event_flood_block = true;
									$( this )
										.trigger( groupAccordionEvent, minimize.toString() );
								}
							);
						}
					);
			}
		);
}

/*****************************************
 ** Event-Based Content Lazyload Widget ***
 *****************************************/
// This widget is a progressive enhancement
// which knows about a specific microformat
// implemented using the HTML5 data-attribute
// The widget automatically replaces content of
// the element the microformat is applied to
// on the event specified as part of the microformat
// Apply the following data attributes to an element to
// utilise this widget:
// 1. content-lazyload-url
// *required
// This is a URL from which to get content
// This content will replace the contents of the
// element that the microformat is applied to
// 2. content-lazyload-event
// *optional
// The event to trigger the content lazyload
// defaults to 'click'
// 3. content-lazyload-target-element
// *optional
// specifies the target element for the loaded content to be
// inserted into

function bindContentLoadOnEvent( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				if ( debug )
					console.log( "bindContentLoadOnEvent processing" );
				// get name of tooltip as contained in data attribute
				var contentUrl = $( this )
					.attr( "data-content-lazyload-url" );
				var contentEvent = $( this )
					.attr( "data-content-lazyload-event" );
				if ( contentEvent === undefined ) {
					contentEvent = "click";
				}
				if ( contentUrl !== "undefined" ) {
					$( this )
						.off(
							contentEvent.toString() + ".content-lazyload"
						)
						.on(
							contentEvent.toString() + ".content-lazyload",
							contentLoader
						);
				}
			}
		);
}

function contentLoader( e ) {
	e.preventDefault();
	this.evt = e;
	var contentUrl = $( this )
		.attr( "data-content-lazyload-url" );
	var contentEvent = $( this )
		.attr( "data-content-lazyload-event" );
	var contentTarget = $( this )
		.attr( "data-content-lazyload-target-element" );
	var append = $( this )
		.attr( "data-content-lazyload-append-content" );
	var persist = $( this )
		.attr( "data-content-lazyload-event-persist" );
	var unbindHandlers = $( this )
		.attr( "data-content-lazyload-unbind-handlers" );
	var onpreload = $( this )
		.attr( "data-content-lazyload-onpreload" );
	var onload = $( this )
		.attr( "data-content-lazyload-onload" );
	var doSpinner = $( this )
		.attr( "data-content-lazyload-show-spinner" );
	var async = $( this )
		.attr( "data-content-lazyload-async" );
	if ( doSpinner === undefined ) {
		doSpinner = false;
	} else {
		doSpinner = doSpinner.toString() === "true" ? true : false;
	}
	if ( contentTarget === undefined ) {
		contentTarget = this;
	} else {
		if ( contentTarget.trim()
			.toLowerCase() === "body" )
			doSpinner = false;
		contentTarget = resolveSelector.run( this, contentTarget.toString() );
	}
	if ( doSpinner ) {
		var spinner = document.createElement( "div" );
		var spinnerChild = document.createElement( "span" );
		$( spinnerChild )
			.html( "&nbsp;" );
		$( spinner )
			.append( spinnerChild );
		$( spinner )
			.attr( "data-loading-spinner", "white" );
		$( contentTarget )
			.empty();
		$( contentTarget )
			.append( spinner );
	}
	if ( onload === undefined ) {
		onload = "";
	} else {
		onload = onload.toString();
	}
	if ( onpreload === undefined ) {
		onpreload = "";
	} else {
		onpreload = onpreload.toString();
	}
	if ( append === undefined ) {
		append = false;
	} else {
		if ( append.toString() === "true" ) {
			append = true;
		} else {
			append = false;
		}
	}
	if ( persist === undefined ) {
		persist = false;
	} else {
		if ( persist.toString() === "true" ) {
			persist = true;
		} else {
			persist = false;
		}
	}
	if ( unbindHandlers === undefined ) {
		unbindHandlers = false;
	} else {
		unbindHandlers = unbindHandlers === "true" ? true : false;
	}
	if ( unbindHandlers )
		$( this )
		.unbind();
	if ( onpreload.length > 0 ) {
		if ( debug )
			console.log( "executing content lazyload onpreload" );
		eval( onpreload );
	}
	if ( async === undefined ) {
		async = true;
	} else {
		async = async.toString()
			.trim() === "true" ? true : false;
	}
	if ( debug )
		console.log( "async: " + async );
	if ( !async ) {
		e.stopImmediatePropagation();
	}
	var self = this;
	$.ajax( {
			url: contentUrl.toString(),
			type: "POST",
			context: self
		} )
		.success(
			function ( msg ) {
				if ( msg.error && debug )
					console.error( msg.error );
				// replace/append html of target element with response from server
				if ( append ) {
					$( contentTarget )
						.append( msg );
				} else {
					$( contentTarget )
						.html( msg );
				}
				if ( debug )
					console.log( "pre bindBehaviour onload check: " + onload );
				if ( onload.indexOf( "bind" ) < 0 )
					bindBehaviours( contentTarget );
				if ( $.browser.msie && ( $.browser.version )
					.charAt( 0 ) === "8" ) {
					Selectivizr.init();
				}
				if ( onload.length > 0 ) {
					if ( debug )
						console.log( "executing content lazyload onload" );
					setTimeout( ( function () {
						eval( onload )
					} ), 50 );
				}
				if ( !async ) {
					this.persist = persist;
					( function ( self, contentEvent, contentLoader ) {
						setTimeout(
							function () {
								$.event.triggerContinue.run( self, self.evt, self.evt.data, self.evt.target, "content-lazyload" );
								if ( !self.persist )
									$( self )
									.off( contentEvent + ".content-lazyload", contentLoader );
							},
							50
						);
					} )( this, contentEvent, contentLoader );
				} else if ( !persist ) {
					$( this )
						.off( contentEvent + ".content-lazyload", contentLoader );
				}
			}
		)
		.error(
			function ( msg, data ) {
				if ( debug )
					console.error( msg );
			}
		);
}

/*****************************************
 ******** Countdown Timer Widget **********
 *****************************************/
// Countdown Timer Widget is a progressive enhancement
// which knows about a specific microformat
// implemented using the HTML5 data-attribute
// The countdown timer automatically replaces the content
// of whichever HTML element has the microformat applied
// Apply the following data attributes to an element to
// utilise this widget:
// 1. data-countdown-timer-target-time
//  *required
//  yyyy,mm,dd,hh,mm
//  Note that the month is 0-11 and time is 24hour format
// 2. data-countdown-timer-display-format
//  *required
//  h:mm:ss
//  This defines whether to show hours, minutes and seconds
//  one letter length says to show only if the value is > 0
//  two letters indicates to display regardless of value
//  omitting a letter will not display that unit type
// 3. data-countdown-timer-thresholds
//  *optional
//  300,60,nn,nnnn,etc
//  these are arbitrary thresholds which result in a
//  data attribute written to the element:
//  data-countdown-timer-threshold-1/2/3/etc
// 4. data-countdown-timer-expired-limit
//  *optional
//  this take a numerical time value and determines
//  for how long the timer will continue into negative
//  values past the target time
// 5. Additional data Attributes for styling:
//   1. data-countdown-timer-hours
//   2. data-countdown-timer-minutes
//   3. data-countdown-timer-seconds
//   4. data-countdown-timer-expired
function bindCountdownTimer( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var targetTime = $( this )
					.attr( "data-countdown-timer-target-time" );
				var expiredLimit = $( this )
					.attr( "data-countdown-timer-expired-limit" );
				var timerResolution = $( this )
					.attr( "data-countdown-timer-resolution" );
				var timerDisplayFormat = $( this )
					.attr( "data-countdown-timer-display-format" );
				var timerThresholds = $( this )
					.attr( "data-countdown-timer-thresholds" );

				if ( typeof timerResolution === "undefined" ) {
					timerResolution = 1000;
				}
				if ( typeof timerThresholds === "undefined" ) {
					timerThresholds = "".toString();
				}
				if ( typeof timerDisplayFormat === "undefined" ) {
					timerDisplayFormat = "h:m:ss";
				}
				if ( typeof expiredLimit === "undefined" ) {
					expiredLimit = 0;
				} else {
					expiredLimit = parseInt( expiredLimit, 10 );
				}
				timerDisplayFormat = timerDisplayFormat.split( ":" );
				timerThresholds = timerThresholds.toString()
					.split( "," );
				// console.dir("timerThresholds first value: " + timerThresholds);
				if ( targetTime !== "undefined" ) {
					targetTime = eval( "new Date(" + targetTime + ").getTime()" );
					var target = this;
					$( this )
						.data(
							"countdown-timer",
							setInterval(
								function () {
									var showHours, showMinutes, showSeconds,
										secs = ( ( targetTime - new Date()
											.getTime() ) / 1000 ),
										hours = 0,
										mins = 0,
										timeLeft = "",
										countdownStatus = "",
										expired = false;
									// console.dir( "total seconds till ground zero: " + secs );
									// initialise
									showHours = showMinutes = showSeconds = 0;
									// console.dir("length: " + timerThresholds.length);
									for ( var i = 0, l = timerThresholds.length; i < l; i++ ) {
										var thresholds = timerThresholds[ i ].toString();
										// console.dir( "Pre thresholds: " + thresholds )
										if ( thresholds.indexOf( "|" ) < 0 ) {
											thresholds += "|" + timerDisplayFormat;
										}
										thresholds = thresholds.toString()
											.split( "|" );
										// console.dir( "Post thresholds: " + thresholds )
										if ( secs <= parseInt( thresholds[ 0 ], 10 ) ) {
											if ( thresholds[ 1 ].indexOf( ":" ) > -1 ) {
												timerDisplayFormat = thresholds[ 1 ].split( ":" );
											}
											// console.dir( "New timerDisplayFormat: " + timerDisplayFormat );
											countdownStatus += " data-countdown-timer-threshold-" + ( i + 1 );
										}
									}
									// console.dir( "timerDisplayFormat: " + timerDisplayFormat );
									for ( var i = 0, l = timerDisplayFormat.length; i < l; i++ ) {
										switch ( timerDisplayFormat[ i ].toLowerCase() ) {
											case "h":
											case "hh":
												showHours = timerDisplayFormat[ i ].length;
												break;
											case "m":
											case "mm":
												showMinutes = timerDisplayFormat[ i ].length;
												break;
											case "s":
											case "ss":
												showSeconds = timerDisplayFormat[ i ].length;
												break;
										}
									}
									// console.dir( "showHours: " + showHours );
									// console.dir( "showMinutes: " + showMinutes );
									// console.dir( "showSeconds: " + showSeconds );
									if ( secs < 0 ) {
										secs *= -1;
										expired = true;
										if ( secs > expiredLimit ) {
											secs = expiredLimit;
											clearInterval( $( target )
												.attr( "data-countdown-timer" ) );
										}
										countdownStatus += " data-countdown-timer-expired";
									}
									if ( showMinutes > 0 || showHours > 0 ) {
										mins = Math.floor( secs / 60 );
										secs = Math.floor( secs % 60 );
									}
									if ( showHours > 0 ) {
										hours = Math.floor( mins / 60 );
										mins = mins % 60;
										if ( hours > 0 || hours == 0 && showHours > 1 )
											timeLeft = "<span data-countdown-timer-hours" + countdownStatus + ">" + hours + "</span>" + timeLeft;
									}
									if ( mins > 0 || mins == 0 && showMinutes > 1 )
										timeLeft += "<span data-countdown-timer-minutes" + countdownStatus + ">" + mins + "</span>";
									if ( showSeconds > 0 )
										timeLeft += "<span data-countdown-timer-seconds" + countdownStatus + ">" + secs + "</span>";
									if ( expired ) {
										timeLeft = "<span" + countdownStatus + ">-</span>" + timeLeft;
									}
									$( target )
										.html(
											timeLeft
										);
								}, timerResolution
							)
						);
				}
			}
		);
};

function bindNumberFormatter( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var fmt = $( this )
					.attr( "data-numeric-display-format" );
				if ( fmt !== undefined ) {
					$( this )
						.off(
							"input.numberFormatter blur.numberFormatter change.numberFormatter"
						)
						.on(
							"input.numberFormatter blur.numberFormatter change.numberFormatter",
							function () {
								$( this )
									.val( format( fmt, $( this )
										.val() ) );
							}
						);
				}
			}
		);
};

// Omniture tracking binding
function bindOmnitureTracking( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var bindEvent = $( this )
					.attr( "data-omniture-tracking-event" );
				var identifier = $( this )
					.attr( "data-omniture-tracking-identifier" );
				if ( bindEvent === undefined ) {
					bindEvent = "click";
				} else {
					bindEvent = bindEvent.toString();
				}
				if ( identifier === undefined ) {
					identifier = $( this )
						.closest( "[data-omniture-tracking-identifier]" );
					if ( identifier.length > 0 ) {
						identifier = $( identifier )
							.attr( "data-omniture-tracking-identifier" );
					}
				}
				if ( identifier !== undefined ) {
					identifier = identifier.toString();
				}
				// legacy from R13.  Refactor R13 page so these values dynamic from data attributes like in R15.
				var prop40 = "";
				if ( identifier === "VALUE" ) {
					prop40 = "value: more info hover";
				} else if ( identifier === "DASHBOARD" ) {
					prop40 = "dashboard: value more info hover";
				} else {
					prop40 = identifier;
				}
				if ( debug )
					console.log( "Omniture tracking identifier: " + identifier );
				if ( identifier !== undefined && bindEvent !== undefined ) {
					$( this )
						.off(
							bindEvent + ".omniture-tracking"
						)
						.on(
							bindEvent + ".omniture-tracking",
							function ( e ) {
								if ( debug ) {
									site_catalyst = 1;
									site_catalyst_un = 1;
									console.log( "Omniture tracking triggered for identifier: " + identifier );
									console.log( "prop40: " + prop40 );
								}
								utils.omn_generic( identifier );
								// utils.omn_track("linkTrackVars","events,prop40", "linkTrackEvents","event19", "prop40", prop40, "events", "event19", "action_code", prop40);
							}
						);
				} else {
					if ( debug ) {
						if ( identifier === undefined )
							console.warn( "Omniture binding failed: missing tracking identifier" );
						if ( bindEvent === undefined )
							console.warn( "Omniture binding failed: missing tracking event" );
					}
				}
			}
		);
};

function showModal( e ) {
	var modalId = $( e.currentTarget )
		.attr( "data-modal-button" );
	$( "[data-modal-toggle]", "[data-modal-popup*='" + modalId + "']" )
		.addClass( "modal_show overlay_show" );
	$( "[data-modal-overlay]", "[data-modal-popup*='" + modalId + "']" )
		.addClass( "overlay_show" );
};

function hideModal( e ) {
	var context = $( e.currentTarget )
		.closest( "[data-modal-popup]" );
	$( "[data-modal-toggle]", context )
		.removeClass( "modal_show overlay_show" );
	$( "[data-modal-overlay]", context )
		.removeClass( "overlay_show" );
	var contentReset = function () {
		var modalContent = context.html();
		$( context )
			.empty();
		$( context )
			.append( modalContent );
		bindBehaviours( context );
	}
	setTimeout( contentReset, 1000 );
};

// bind modal behaviours
function bindModals( root ) {
	if ( root === undefined )
		root = document;
	else
		root = resolveSelector.run( this, root );

	try {
		$( root )
			.find( "[data-modal-button]" )
			.off(
				"click.show-modal"
			)
			.on(
				"click.show-modal",
				showModal
			);
	} catch ( err ) {
		if ( debug )
			console.error( err.toString() );
	}

	try {
		$( root )
			.find( "[data-modal-close]" )
			.off(
				"click.hide-modal",
				hideModal
			)
			.on(
				"click.hide-modal",
				hideModal
			);
	} catch ( err ) {
		if ( debug )
			console.error( err.toString() );
	}
};


/******************************************
 * Fioreign Element Event Listening Widget *
 ******************************************/
// A progressive enhancement
// which knows about a specific microformat
// implemented using the HTML5 data-attribute
// The Foreign Event Listener sends events from one element to another
// Apply the following data attributes to an element to
// utilise this widget:
// 1. data-listen-event-type
// 	a comma-separated list of named events. eg. mouseover,mouseout
// 2. data-listen-event-target-selectors
//	a comma-separated list of element selectors to listen for events on

function bindRemoteEventListeners( eventTypeSelectorAttributeName, eventTargetSelectorAttributeName, root ) {
	if ( root === undefined )
		root = document;
	$( root )
		.find( eventTypeSelectorAttributeName )
		.each(
			function () {
				var tgt = this,
					evts, evtSources;
				if ( eventTypeSelectorAttributeName === undefined )
					eventTypeSelectorAttributeName = "listen-event-type";
				else
					eventTypeSelectorAttributeName = eventTypeSelectorAttributeName.replace( "[data-", "" )
					.replace( "]", "" );
				evts = $( this )
					.data( eventTypeSelectorAttributeName );
				if ( evts === undefined ) {
					if ( debug )
						console.warn( "RemoteEventListeners missing event type(s)" );
					if ( debug )
						console.groupEnd( "bindRemoteEventListeners" );
					return;
				} else {
					evts = evts.toString()
						.split( "," );
				}
				if ( eventTargetSelectorAttributeName === undefined )
					eventTargetSelectorAttributeName = "listen-event-target-selectors";
				else
					eventTargetSelectorAttributeName = eventTargetSelectorAttributeName.replace( "[data-", "" )
					.replace( "]", "" );
				evtSources = $( this )
					.data( eventTargetSelectorAttributeName );
				if ( evtSources === undefined ) {
					if ( debug )
						console.warn( "RemoteEventListeners missing target selector(s)" );
					if ( debug )
						console.groupEnd( "bindRemoteEventListeners" );
					return;
				} else {
					evtSources = evtSources.toString()
						.split( "," );
				}
				// var bindElm;
				for ( var ev = 0, evl = evts.length; ev < evl; ev++ ) {
					if ( evts[ ev ].length > 0 ) {
						if ( debug )
							console.log( "RemoteEventListeners event: " + evts[ ev ] );
						for ( var i = 0, l = evtSources.length; i < l; i++ ) {
							try {
								if ( evtSources[ i ].length > 0 ) {
									if ( debug )
										console.log( "RemoteEventListeners source selector: " + evtSources[ i ] );
									var bindElm = resolveSelector.run( this, evtSources[ i ] );
									if ( $( bindElm )
										.length > 0 ) {
										$( bindElm )
											.off(
												evts[ ev ] + ".remote-event-listeners"
											)
											.on(
												evts[ ev ] + ".remote-event-listeners",
												function ( e ) {
													if ( debug )
														console.log( "triggering event '" + e.type + "' for selector '" + tgt + "'" );
													var t = tgt;
													// e.override_event_flood_block = true;
													if ( debug )
														console.log( "e.remote_event_processed: " + e.remote_event_processed )
													e.remote_event = true;
													if ( e.remote_event_processed === undefined ) {
														e.remote_event_processed = true;
														$( tgt )
															.trigger( e, "remote-event" );
													}
												}
											);
									}
								}
							} catch ( err ) {
								console.error( err.toString() );
							}
						}
					}
				}
			}
		);
}

/*****************************************
 ********* RICH HTML Select Control *******
 *****************************************/
// A progressive enhancement
// which knows about a specific microformat
// implemented using the HTML5 data-attribute
// Uses Unordered list to create a drop-down
// select form control
// @dependsOn - data-accordion widget
// @dependsOn - data-listen-event-type widget
// To utilise this widget, the following attributes are available:
// 1. data-listen-event-type
// 	a comma-separated list of named events. eg. mouseover,mouseout
// 2. data-listen-event-target-selectors
//	a comma-separated list of element selectors to listen for events on
function bindListSelectBoxes( root, initAccordions ) {
	if ( root === undefined )
		root = document;
	$( root )
		.find( "[data-selection-list]" )
		.each(
			function () {
				$( this )
					.attr( "data-initial-select-value", $( this )
						.find( "[data-selection-list-item-selected]" )
						.outerHTML() );
			}
		)
		.off(
			"click.select-widget action.select-widget"
		)
		.on(
			"click.select-widget action.select-widget",
			function ( e ) {
				if ( debug )
					console.log( e.type + " triggered" );
				var resetOnOpen = $( this )
					.attr( "data-selection-list-reset-on-open" ) !== "false" ? true : false;
				if ( debug )
					console.log( "reset on open: " + resetOnOpen );
				if ( resetOnOpen && $( this )
					.find( "[data-accordion]" )
					.attr( "data-accordion-state" ) === "collapse" ) {
					$( this )
						.trigger( "change" );
					$( this )
						.trigger( "reset.select-widget" );
				}
				$( this )
					.find( "[data-accordion]" )
					.trigger( e, "accordion-action.accordion" );
				$( document )
					.trigger( "click.select-widget-blur", this );
			}
		)
		// on blur close any selection list drop-downs
		.off(
			"blur.select-widget"
		)
		.on(
			"blur.select-widget",
			function ( e, exclude ) {
				if ( debug )
					console.log( "blur triggered for data-selection-list" );
				// $( this ).find( "*" ).css( "background-color", "#f00" );
				if ( exclude === undefined || exclude !== undefined && exclude !== this )
					$( this )
					.find( "[data-accordion]" )
					.trigger( "accordion-action.accordion", "true" );
			}
		)
		// reset functionality - reset to initial state, optionally destroying the select widget's contents
		.off(
			"reset.select-widget"
		)
		.on(
			"reset.select-widget",
			function ( e, destroyList ) {
				if ( debug )
					console.log( "reset triggered for data-selection-list" );
				$( this )
					.find( "[data-selection-list-item-selected]" )
					.replaceWith( $( this )
						.attr( "data-initial-select-value" ) );
				$( this )
					.find( "input" )
					.remove();
				if ( destroyList !== undefined && destroyList ) {
					if ( debug )
						console.log( "destroying list" );
					$( this )
						.find( ".panel__body" )
						.empty();
				}
			}
		)
		// set selection programmatically
		.off(
			"set-selection.select-widget"
		)
		.on(
			"set-selection.select-widget",
			function ( e, selectionValue ) {
				if ( debug )
					console.log( "set-selection triggered for data-selection-list with value: " + selectionValue );
				var selection = $( this )
					.find( "[data-selection-list-item-value='" + selectionValue + "']" );
				if ( selection.length ) {
					$( this )
						.find( "[data-selection-list-item-selected]" )
						.attr( "data-selection-list-item-selected", selectionValue );
					$( this )
						.find( "[data-selection-list-item-selected]" )
						.html( $( selection )
							.html() );
				}
			}
		)
		// select action - copy value from selected list item to hidden form field - use just like a regular form
		.find( "[data-selection-list-item-value]" )
		.off(
			"change.select-widget click.select-widget-item"
		)
		.on(
			"change.select-widget click.select-widget-item",
			function ( e ) {
				if ( debug )
					console.log( "selection list item clicked" );
				if ( debug )
					console.log( e );
				var isChanged = false;
				var sel = $( this )
					.closest( "[data-selection-list]" )
					.find( "[data-selection-list-item-selected]" );
				$( sel )
					.html( $( this )
						.html() );
				bindBehaviours.run( this, sel );
				$( sel )
					.attr( "data-selection-list-item-selected", $( this )
						.attr( "data-selection-list-item-value" ) );
				var list = $( this )
					.closest( "[data-selection-list]" );
				var input;
				// if form hidden input doesn't exist create and copy value from selected item, else just copy value
				if ( $( list )
					.find( "input[name*='" + $( list )
						.attr( "data-selection-list" ) + "']" )
					.length < 1 ) {
					input = document.createElement( "input" );
					$( input )
						.attr( "name", $( list )
							.attr( "data-selection-list" ) )
						.attr( "type", "hidden" )
						.attr( "value", $( sel )
							.attr( "data-selection-list-item-selected" ) );
					$( list )
						.append( input );
					isChanged = true;
				} else {
					input = $( list )
						.find( "input[name*='" + $( list )
							.attr( "data-selection-list" ) + "']" );
					if ( $( input )
						.attr( "value" ) !== $( sel )
						.attr( "data-selection-list-item-selected" ) )
						isChanged = true;
					$( input )
						.attr( "value", $( sel )
							.attr( "data-selection-list-item-selected" ) );
				}
				if ( debug )
					console.log( "isChanged: " + isChanged );
				var selList = $( this )
					.closest( "[data-selection-list]" );
				if ( debug )
					console.log( "selection-list-onchange: " + $( selList )
						.attr( "data-selection-list-onchange" ) );
				if ( isChanged && $( selList )
					.attr( "data-selection-list-onchange" ) !== undefined && $( selList )
					.attr( "data-selection-list-onchange" ) !== "" ) {
					if ( debug )
						console.log( "onchange" );
					var onchange = $( selList )
						.attr( "data-selection-list-onchange" );
					execStringWithScope( onchange, selList, 50 );
				}
				selList.trigger( "blur.select-widget" );
				e.stopPropagation();
			}
		);

	// when clicking outside selection list send blur event to selection lists
	$( document )
		.off(
			"click.select-widget-blur"
		)
		.on(
			"click.select-widget-blur",
			function ( e, exclude ) {
				( function ( exclude ) {
					$( "[data-selection-list]" )
						.each(
							function () {
								$( this )
									.trigger( "blur.select-widget", exclude );
							}
						);
				} )( exclude );
			}
		);

	if ( initAccordions === undefined || initAccordions ) {
		if ( debug )
			console.log( "accordion init triggered" );
		$( root )
			.find( "[data-selection-list] [data-accordion]" )
			.trigger( "accordion-action.accordion", "init" );
	}
}

/*****************************************
 ************ Tab Menu  Control ***********
 *****************************************/
// A progressive enhancement
// which knows about a specific microformat
// implemented using the HTML5 data-attribute
// Uses Unordered list to create a tabbed menu
// select form control
// @dependsOn - data-accordion widget
// @dependsOn - data-listen-event-type widget
// To utilise this widget, the following attributes are available:
// 1. data-listen-event-type
// 	a comma-separated list of named events. eg. mouseover,mouseout
// 2. data-listen-event-target-selectors
//	a comma-separated list of element selectors to listen for events on
function bindTabNavBehaviour( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				$( this )
					.on(
						"click",
						function ( e ) {
							e.preventDefault();
							if ( debug )
								console.log( "tab nav item '" + $( this )
									.html() + "' at position " + $( this )
									.parent()
									.find( "[data-tab-nav-item]" )
									.index( this ) + " clicked" );
							$( this )
								.parent()
								.find( "[data-tab-nav-item]" )
								.each(
									function () {
										$( this )
											.attr( "data-tab-nav-item", "" );
									}
								);
							$( this )
								.attr( "data-tab-nav-item", "active" );
							var index = $( this )
								.parent()
								.find( "[data-tab-nav-item]" )
								.index( this );
							if ( index === undefined )
								index = 0;
							$( this )
								.closest( "[data-tab-nav]" )
								.attr( "data-tab-nav", index );
							$( this )
								.closest( "[data-tab-nav]" )
								.find( "[data-tab-nav-content]" )
								.each(
									function () {
										$( this )
											.attr( "data-tab-nav-content", "" );
									}
								);
							if ( debug )
								console.log( "number of content blocks found: " + $( this )
									.parent()
									.parent()
									.find( "[data-tab-nav-content]" )
									.length );
							$( $( this )
									.closest( "[data-tab-nav]" )
									.find( "[data-tab-nav-content]" )
									.get( index ) )
								.attr( "data-tab-nav-content", "active" );
						}
					);
			}
		);
}

function bindBetButtons( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var marketId = $( this )
					.attr( "data-market-id" ),
					marketEventBind = ( this )
					.attr( "data-market-price-bind-event" ),
					marketEventOutcomeId = $( this )
					.attr( "data-market-event-outcome-id" ),
					marketEventOutcomeVariantId = $( this )
					.attr( "data-market-event-outcome-variant-id" ),
					marketLivePriceNumerator = $( this )
					.attr( "data-market-live-price-numerator" ),
					marketLivePriceDenominator = $( this )
					.attr( "data-market-live-price-denominator" ),
					marketOutcomeHandicapValue = $( this )
					.attr( "data-market-outcome-handicap-value" );
				if (
					marketId === ( undefined || "" ) ||
					marketEventOutcomeId === ( undefined || "" ) ||
					marketEventOutcomeVariantId === ( undefined || "" ) ||
					marketLivePriceNumerator === ( undefined || "" ) ||
					marketLivePriceDenominator === ( undefined || "" ) ||
					marketOutcomeHandicapValue === ( undefined || "" )
				) {
					if ( debug )
						console.warn( "Market price button missing data.  Set to disabled" );
					$( this )
						.attr( "disabled", "true" );
				}
				if ( marketEventBind === undefined )
					marketEventBind = "click.bet-button";
				else
					marketEventBind += ".bet-button";
				// create class strings betslip.js looks for
				// replace betslip.js here when possible.
				var priceBox = "price-box_" + marketEventOutcomeId + "_" + marketEventOutcomeVariantId;
				var priceStr = "price-str_" + marketId + "_" + marketLivePriceNumerator + "_" + marketLivePriceDenominator + "_" + marketOutcomeHandicapValue;
				var ocvStr = "ocv-str_" + marketEventOutcomeVariantId + "_" + marketLivePriceNumerator + "_" + marketLivePriceDenominator;
				var priceMkt = "price-mkt_" + marketId;
				$( this )
					.addClass( priceBox )
					.addClass( priceStr )
					.addClass( ocvStr )
					.addClass( priceMkt )
					.off( marketEventBind )
					.on(
						marketEventBind,
						function ( e ) {
							try {
								betslip.onclick_add_bet( marketEventOutcomeId, '-1' );
							} catch ( err ) {
								if ( debug )
									console.warn( "Error adding market " + marketEventOutcomeId + " to BetSlip" );
							}
						}
					);
			}
		);
}

/*****************************************
 ********** Toggle Class Control **********
 *****************************************/
// A progressive enhancement to enable class
// toggle on event
// Use to show/hide elements, etc.
// To utilise this widget, the following attributes are available:
// 1. data-toggle-class
// 	a |-separated array of class names. eg. show|hide
// 2. data-toggle-class-selectors
//	a |-separated array of target element selectors
//  to apply the corresponding class array element to
// 3. data-toggle-class-event
//  the event that triggers the class toggle

function bindToggleClass( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var classes = $( this )
					.attr( "data-toggle-class" ),
					targetSelectors = $( this )
					.attr( "data-toggle-class-selectors" ),
					toggleEvent = $( this )
					.attr( "data-toggle-class-event" );
				if ( targetSelectors === undefined ) {
					if ( debug )
						console.warn( "missing target selectors" );
					return false;
				} else {
					targetSelectors = targetSelectors.toString()
						.split( "|" );
				}
				if ( classes === undefined ) {
					if ( debug )
						console.warn( "missing classes" );
					return false;
				} else {
					classes = classes.toString()
						.split( "|" );
				}
				if ( toggleEvent === undefined ) {
					toggleEvent = "click";
				} else {
					toggleEvent = toggleEvent.toString();
				}
				toggleEvent += ".toggle-class";
				if ( targetSelectors.length !== classes.length ) {
					if ( debug )
						console.warn( "Toggle class has a selectors to classes number mismatch.  Exiting" );
					return;
				}
				$( this )
					.off(
						toggleEvent
					)
					.on(
						toggleEvent,
						function ( e ) {
							for ( var i = 0, il = targetSelectors.length; i < il; i++ ) {
								if ( debug )
									console.log( "toggleClass triggered" );
								if ( debug )
									console.log( "toggleClass triggered for selector(s): " + targetSelectors[ i ] );
								if ( debug )
									console.log( "toggleClass selectors found: " + resolveSelector.run( this, targetSelectors[ i ] )
										.length );
								resolveSelector.run( this, targetSelectors[ i ] )
									.each(
										function () {
											if ( debug )
												console.log( "For object: " + this + "\nToggling class: " + classes[ i ] );
											$( this )
												.toggleClass( classes[ i ] );
										}
									);
							}
						}
					);
			}
		);
}

/*****************************************
 ********** Add Class Control **********
 *****************************************/
// A progressive enhancement to enable class
// toggle on event
// Use to show/hide elements, etc.
// To utilise this widget, the following attributes are available:
// 1. data-toggle-class
// 	a |-separated array of class names. eg. show|hide
// 2. data-toggle-class-selectors
//	a |-separated array of target element selectors
//  to apply the corresponding class array element to
// 3. data-toggle-class-event
//  the event that triggers the class toggle

function bindAddClass( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var classes = $( this )
					.attr( "data-add-class" ),
					targetSelectors = $( this )
					.attr( "data-add-class-selectors" ),
					addEvent = $( this )
					.attr( "data-add-class-event" );
				if ( targetSelectors === undefined ) {
					if ( debug )
						console.warn( "missing target selectors" );
					return false;
				} else {
					targetSelectors = targetSelectors.toString()
						.split( "|" );
				}
				if ( classes === undefined ) {
					if ( debug )
						console.warn( "missing classes" );
					return false;
				} else {
					classes = classes.toString()
						.split( "|" );
				}
				if ( addEvent === undefined ) {
					addEvent = "click";
				} else {
					addEvent = addEvent.toString();
				}
				addEvent += ".add-class";
				if ( targetSelectors.length !== classes.length ) {
					if ( debug )
						console.warn( "Add class has a selectors to classes number mismatch.  Exiting" );
					return;
				}
				$( this )
					.off(
						addEvent
					)
					.on(
						addEvent,
						function ( e ) {
							for ( var i = 0, il = targetSelectors.length; i < il; i++ ) {
								if ( debug )
									console.log( "addClass triggered for selector(s): " + targetSelectors[ i ] );

								$( resolveSelector.run( this, targetSelectors[ i ] ) )
									.each(
										function () {
											$( this )
												.addClass( classes[ i ] );
										}
									);
							}
						}
					);
			}
		);
}

/*****************************************
 ********** Remove Class Control **********
 *****************************************/
// A progressive enhancement to enable class
// toggle on event
// Use to show/hide elements, etc.
// To utilise this widget, the following attributes are available:
// 1. data-toggle-class
// 	a |-separated array of class names. eg. show|hide
// 2. data-toggle-class-selectors
//	a |-separated array of target element selectors
//  to apply the corresponding class array element to
// 3. data-toggle-class-event
//  the event that triggers the class toggle

function bindRemoveClass( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var classes = $( this )
					.attr( "data-remove-class" ),
					targetSelectors = $( this )
					.attr( "data-remove-class-selectors" ),
					removeEvent = $( this )
					.attr( "data-remove-class-event" );
				if ( targetSelectors === undefined ) {
					if ( debug )
						console.warn( "missing target selectors" );
					return false;
				} else {
					targetSelectors = targetSelectors.toString()
						.split( "|" );
				}
				if ( classes === undefined ) {
					if ( debug )
						console.warn( "missing classes" );
					return false;
				} else {
					classes = classes.toString()
						.split( "|" );
				}
				if ( removeEvent === undefined ) {
					removeEvent = "click";
				} else {
					removeEvent = removeEvent.toString();
				}
				removeEvent += ".remove-class"
				if ( targetSelectors.length !== classes.length ) {
					if ( debug )
						console.warn( "Toggle class has a selectors to classes number mismatch.  Exiting" );
					return;
				}
				$( this )
					.off(
						removeEvent
					)
					.on(
						removeEvent,
						function ( e ) {
							for ( var i = 0, il = targetSelectors.length; i < il; i++ ) {
								if ( debug )
									console.log( "bindRemoveClass triggered" );
								$( resolveSelector.run( this, targetSelectors[ i ] ) )
									.each(
										function () {
											$( this )
												.removeClass( classes[ i ] );
										}
									);
							}
						}
					);
			}
		);
}


function bindEventDependency( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var deps = $( this )
					.attr( "data-event-dependency" );
				if ( !deps.length ) {
					return;
				}
				var elementData = $._data( this );
				var events = ( elementData.events ? elementData.events : elementData.__events__ ),
					deps = deps.toString()
					.split( "," );
				var handlers, dep, depEventType, depSrc, depTgt;
				// Only one handler. Nothing to change.
				for ( var i = 0, il = deps.length; i < il; i++ ) {
					depSrc = false;
					depTgt = false;
					dep = deps[ i ].split( "=" );
					depEventType = dep[ 0 ].split( "." )[ 0 ];
					dep[ 0 ] = dep[ 0 ].replace( "." + depEventType, "" );
					dep[ 1 ] = dep[ 1 ].replace( "." + depEventType, "" );

					handlers = events[ depEventType ];
					if ( handlers.length == 1 ) {
						return;
					}
					if ( debug )
						console.log( handlers );
					for ( var j = 0, jl = handlers.length; j < jl && !( depSrc && depTgt ); j++ ) {
						if ( debug )
							console.log( handlers[ j ].namespace );
						if ( dep[ 0 ] === handlers[ j ].namespace ) {
							dep[ 0 ] = handlers.splice( j, 1 );
							depSrc = true;
						} else if ( dep[ 1 ] === handlers[ j ].namespace ) {
							dep[ 1 ] = handlers.splice( j, 1 );
							depTgt = true;
						}
					}
					if ( depSrc && depTgt ) {
						handlers.push( dep[ 1 ] );
						handlers.push( dep[ 0 ] );
					}
				}
			}
		);
}


function bindJumpLocation( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var target = $( this )
					.attr( "data-jump-location" );
				var evt = $( this )
					.attr( "data-jump-location-event" );
				if ( target === undefined ) {
					if ( debug ) {
						console.warn( "missing target selector" );
					}
					return;
				} else {
					target = resolveSelector.run( this, target.toString() );
				}
				if ( evt === undefined ) {
					evt = "click.jump-location"
				} else {
					evt = evt.toString() + ".jump-location";
				}
				$( this )
					.off(
						evt
					)
					.on(
						evt,
						function ( e ) {
							jumpTo( target );
						}
					);
			}
		);
}

function bindMoveContent( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				var targetLocation = $( this )
					.attr( "data-move-content-target" );
				var moveEvent = $( this )
					.attr( "data-move-content-event" );
				var moveOpType = $( this )
					.attr( "data-move-content-operation-type" );
				if ( targetLocation === undefined ) {
					if ( debug )
						console.warn( "missing target location for content move" );
				} else {
					targetLocation = resolveSelector.run( this, targetLocation.toString() );
				}
				if ( moveEvent === undefined ) {
					switch ( moveOpType.toString() ) {
						case 'prepend':
							targetLocation.prepend( $( this )
								.detach() );
							break;
						case 'append':
						default:
							targetLocation.append( $( this )
								.detach() );
							break;
					}
					$( this )
						.removeAttr( "data-move-content-target" )
						.removeAttr( "data-move-content-event" )
						.removeAttr( "data-move-content-operation-type" );
				} else {
					$( this )
						.off(
							moveEvent + ".move-content"
						)
						.on(
							moveEvent + ".move-content",
							function ( e ) {
								switch ( moveOpType.toString() ) {
									case 'prepend':
										targetLocation.prepend( $( this )
											.detach() );
										break;
									case 'append':
									default:
										targetLocation.append( $( this )
											.detach() );
										break;
								}
								$( this )
									.removeAttr( "data-move-content-target" )
									.removeAttr( "data-move-content-event" )
									.removeAttr( "data-move-content-operation-type" );
							}
						);
				}
			}
		);
}

function bindLegacyBrowserSupport( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	console.log( "legacy support blocks found: " + $( selector )
		.length );
	if ( typeof Selectivizr !== undefined )
		Selectivizr.init( selected );
}

function unbindTetherOnScroll( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				selector = selector.replace( /[\[\]\$]/gi, "" );
				if ( debug )
					console.log( "Un-binding tether to viewport on scroll for selector: " + selector );
				$( window )
					.off(
						"scroll-viewport.tether-to-viewport" + "." + selector
					);
				$( this )
					.css( "position", $( this )
						.attr( "data-original-position-type" ) );
			}
		);
}

function bindTetherOnScroll( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				selector = selector.replace( /[\[\]\$]/gi, "" );
				if ( debug )
					console.log( "Binding tether to viewport on scroll for selector: " + selector );
				( function ( target ) {
					$( window )
						.off(
							"resize scroll-viewport.tether-to-viewport" + "." + selector
						)
						.on(
							"resize scroll-viewport.tether-to-viewport" + "." + selector,
							function ( e ) {
								$( target )
									.trigger( "tether-to-viewport" );
							}
						);
				} )( this );
			}
		);
}

function unbindTetherToViewport( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				if ( debug )
					console.log( "Un-binding tether to viewport" );
				$( this )
					.off(
						"tether-to-viewport"
					);
				$( this )
					.css( "position", $( this )
						.attr( "data-original-position-type" ) );
				$( this )
					.removeAttr( "data-original-vertical-position" );
			}
		);
}

function bindTetherToViewport( selector, root ) {
	if ( root === undefined )
		root = document;
	var selected = resolveSelector.run( this, selector );
	$( root )
		.find( selected )
		.each(
			function () {
				if ( debug ) {
					console.log( "Binding tether to viewport" );
					console.log( "tether parameters: " + $( this )
						.attr( selector ) );
				}
				if ( $( this )
					.attr( selector ) === "suppress" )
					return;
				$( this )
					.off(
						"tether-to-viewport"
					)
					.on(
						"tether-to-viewport",
						function ( e ) {
							if ( debug )
								console.log( "tether-viewport triggered" );
							var tetherOrientation = $( this )
								.attr( "data-tether-to-viewport" );
							var tetherVerticalPosition = $( this )
								.attr( "data-tether-to-viewport-vertical-position" );
							var tetherHorizontalPosition = $( this )
								.attr( "data-tether-to-viewport-horizontal-position" );
							var tetherReferenceContainer = $( this )
								.attr( "data-tether-to-viewport-reference-container" );
							var tetherClass = $( this )
								.attr( "data-tether-to-viewport-action-class" );
							var tetherAnimateTransition = $( this )
								.attr( "data-tether-to-viewport-animate-transition" );
							var tetherUp = false;
							var tetherDown = false;
							var tetherLeft = false;
							var tetherRight = false;
							if ( tetherOrientation === undefined ) {
								tetherOrientation = "vertical";
							} else {
								tetherOrientation = tetherOrientation.toString();
								switch ( tetherOrientation.toLowerCase() ) {
									case "vertical":
										tetherUp = true;
										tetherDown = true;
										break;
									case "up":
										tetherUp = true;
										break;
									case "down":
										tetherDown = true;
										break;
									case "horizontal":
										tetherLeft = true;
										tetherRight = true;
										break;
									case "left":
										tetherLeft = true;
										break;
									case "right":
										tetherRight = true;
										break;
									case "all":
										tetherUp = true;
										tetherDown = true;
										tetherLeft = true;
										tetherRight = true;
										break;
									default:
										if ( debug )
											console.warn( "unknown orientation value: " + tetherOrientation );
										return;
										break;
								}
							}
							if ( tetherVerticalPosition === undefined ) {
								tetherVerticalPosition = "top";
							}
							tetherVerticalPosition = tetherVerticalPosition.toString();
							switch ( tetherVerticalPosition.toString() ) {
								case "top":
									tetherVerticalPosition = .0001;
									break;
								case "bottom":
									tetherVerticalPosition = .9999;
									break;
								case "center":
									tetherVerticalPosition = .5;
									break;
								default:
									if ( tetherVerticalPosition.indexOf( "%" ) )
										tetherVerticalPosition = parseInt( tetherVerticalPosition.replace( "%", "" ), 10 ) / 100;
									break;
							}
							if ( tetherHorizontalPosition === undefined ) {
								tetherHorizontalPosition = "left";
							}
							tetherHorizontalPosition = tetherHorizontalPosition.toString();
							switch ( tetherHorizontalPosition.toString() ) {
								case "left":
									tetherHorizontalPosition = .0001;
									break;
								case "right":
									tetherHorizontalPosition = .9999;
									break;
								case "center":
									tetherHorizontalPosition = .5;
									break;
								default:
									if ( tetherHorizontalPosition.indexOf( "%" ) )
										tetherHorizontalPosition = parseInt( tetherHorizontalPosition.replace( "%", "" ), 10 ) / 100;
									break;
							}
							if ( tetherReferenceContainer !== undefined ) {
								tetherReferenceContainer = resolveSelector.run( this, tetherReferenceContainer.toString() );
							}
							if ( tetherReferenceContainer === undefined || tetherReferenceContainer.length < 1 ) {
								tetherReferenceContainer = $( this )
									.offsetParent();
							}
							if ( tetherClass !== undefined ) {
								tetherClass = tetherClass.toString();
							}
							if ( tetherAnimateTransition === undefined ) {
								tetherAnimateTransition = true;
							} else {
								tetherAnimateTransition = tetherAnimateTransition.toString()
									.toLowerCase() === "false" ? false : true;
							}
							var selfViewport = {
								top: $( this )
									.offset()
									.top,
								left: $( this )
									.offset()
									.left,
								width: $( this )
									.width(),
								height: $( this )
									.height()
							};
							if ( debug ) {
								console.log( "selfViewport:" );
								console.log( selfViewport );
							}
							var viewport = {
								top: $( document )
									.scrollTop(),
								left: $( document )
									.scrollLeft(),
								width: $( window )
									.width(),
								height: $( window )
									.height()
							}
							if ( debug ) {
								console.log( "viewport:" );
								console.log( viewport );
							}
							var verticalThreshold;

							// console.log( "viewport.height: " + viewport.height )
							// console.log( "selfViewport.height: " + selfViewport.height )
							if ( $( this )
								.css( "position" ) !== "fixed" && $( this )
								.css( "position" ) !== "absolute" ) {
								verticalThreshold = Math.abs( selfViewport.top - ( viewport.height - selfViewport.height ) ); // / 2 );
								$( this )
									.attr( "data-original-vertical-position", verticalThreshold );

								// var visualVerticalThresholdIndicator2 = $( "<div>&nbsp;</div>" )
								// .css(
								// 	{
								// 		position: "absolute",
								// 		height: "1px",
								// 		width: "100%",
								// 		top: selfViewport.top + "px",
								// 		left: "0",
								// 		border: "1px solid #f0f",
								// 		zIndex: "9999",
								// 		backgroundColor: "#f00"
								// 	}
								// );
								// $( "body" ).append( visualVerticalThresholdIndicator2 );
							} else {
								verticalThreshold = parseInt( $( this )
									.attr( "data-original-vertical-position" ), 10 )
							}
							var horizontalThreshold = selfViewport.left + ( tetherHorizontalPosition * ( viewport.width - selfViewport.width ) );
							if ( debug ) {
								console.log( "viewport.top: " + viewport.top );
								console.log( "verticalThreshold: " + verticalThreshold );
							}
							// // Display the vertical threshold on screen for visual verification
							// var visualVerticalThresholdIndicator = $( "<div>&nbsp;</div>" )
							// 	.css(
							// 		{
							// 			position: "absolute",
							// 			height: "1px",
							// 			width: "100%",
							// 			top: verticalThreshold + "px",
							// 			left: "0",
							// 			border: "1px solid #f00",
							// 			zIndex: "9999",
							// 			backgroundColor: "#f00"
							// 		}
							// 	);
							// $( "body" ).append( visualVerticalThresholdIndicator );

							if ( tetherUp ) {
								if ( viewport.top < verticalThreshold ) {
									if ( $( this )
										.attr( "data-original-position-type" ) === undefined )
										$( this )
										.attr( "data-original-position-type", $( this )
											.css( "position" ) );
									if ( tetherClass !== undefined ) {
										$( this )
											.addClass( tetherClass );
									} else {
										$( this )
											.css( "position", "fixed" );
										if ( $( this )
											.css( "width" ) === "auto" )
											$( this )
											.css( "width", selfViewport.width + "px" );
									}
								} else {
									if ( tetherClass !== undefined ) {
										$( this )
											.removeClass( tetherClass );
									} else {
										$( this )
											.css( "position", $( this )
												.attr( "data-original-position-type" ) );
									}
									$( this )
										.removeAttr( "data-original-position-type" );
								}
							}
						}
					);
			}
		);
}

/*********************************
 ***** END PROGRESSIVE WIDGETS ****
 *********************************/

function bindBehaviours( root ) {

	if ( root === undefined )
		root = document;

	bindOmnitureTracking.run( this, "[data-omniture-tracking-identifier]", root );

	bindCountdownTimer.run( this, "[data-countdown-timer-target-time]", root );

	bindContentLoadOnEvent.run( this, "[data-content-lazyload-url]", root );

	bindNumberFormatter.run( this, "[data-numeric-display-format]", root );

	bindAccordionBehaviour.run( this, "[data-accordion]", root );

	bindAccordionGroupBehaviour.run( this, "[data-accordion-group-toggle]", "[data-accordion-group-root-element-selector]", root );

	bindLinkValues.run( this, "[data-listen-event-target-selectors]", root );

	bindValueFromFormula.run( this, "[data-value-from-formula]", root );

	bindTabNavBehaviour.run( this, "[data-tab-nav-item]", root );

	// legacy naming support
	bindRemoteEventListeners.run( this, "[data-link-event-type]", "[data-link-event-elements]", root );
	bindRemoteEventListeners.run( this, "[data-listen-event-type]", "[data-listen-event-target-selectors]", root );

	bindModals.run( this, root );

	bindListSelectBoxes.run( this, root );

	bindToggleClass.run( this, "[data-toggle-class]", root );

	bindAddClass.run( this, "[data-add-class]", root );

	bindRemoveClass.run( this, "[data-remove-class]", root );

	bindJumpLocation.run( this, "[data-jump-location]", root );

	bindMoveContent.run( this, "[data-move-content-target]", root );

	bindTetherToViewport.run( this, "[data-tether-to-viewport]", root );

	bindTetherOnScroll.run( this, "[data-tether-to-viewport]", root );

	// bindLegacyBrowserSupport.run( this, "[data-legacy-support]", root );

	bindEventDependency.run( this, "[data-event-dependency]", root );

}


/*****************************
 **** SCRIPT INITIALIZATION****
 *****************************/

// bind behaviours onload of document
$( document )
	.ready(
		function () {

			/* Set Debug State */

			if ( location.href.indexOf( "www." ) < 0 )
				debug = true;
			else
				debug = false;

			/* Generic Widgets Setup */
			debug = true;
			bindBehaviours.run( this );

		}
	);

// set HTML data attribute with user agent string for css targetting
var doc = document.documentElement;
doc.setAttribute( 'data-useragent', navigator.userAgent );

// fix touch events
if ( isMobile.any() ) {
	try {
		document.addEventListener( "touchstart", function () {}, true );
		// $(document).on( "vmouseover", function () { } );
	} catch ( err ) {}
}

// $(window).load(
// 	function()
// 	{
// 		setTimeout( rebindWindowScroll, 2000 );
// 	}
// );

// var rebindWindowScroll = function()
// {
// 	$(window).off("scroll");
// 	var scrollTimeout;  // global for any pending scrollTimeout
// 	$(window).scroll(function () {
// 	    if (scrollTimeout) {
// 	        // clear the timeout, if one is pending
// 	        clearTimeout(scrollTimeout);
// 	        scrollTimeout = null;
// 	    }
// 	    scrollTimeout = setTimeout(vpos_betslip, 250);
// 	});
// }

// window.onerror=function(){return true}

// data-event-dependency="click.toggle-class=click.content-lazyload"

// console.groupCollapsed = function(){return}
// console.groupEnd = function(){return}

// Proxy method to provide house-keeping capabilities for Function execution
( function () {
	if( typeof printStackTrace === "undefined" )
	{
		function printStackTrace()
		{
			return getArgsArray( arguments );
		}
	}
	window.onerror=function( e ){
		// if( debug )
		// {
			// var trace = printStackTrace({e: e});
	 			// console.warn( "Error:\n" + e.message + "\nStack trace:\n" + trace.join( "\n" ) );
	 		// }
	 		// return true;
	}
	Function.prototype.run = function () {
		var __name;
		if ( this.toString()
			.match( /function\s(.*)\(/ ) !== null && this.toString()
			.match( /function\s(.*)\(/ )
			.length > 1 && this.toString()
			.match( /function\s(.*)\(/ )[ 1 ] !== "" )
			__name = this.toString()
			.match( /function\s(.*)\(/ )[ 1 ];
		else if ( arguments.length > 2 )
			__name = arguments[ 2 ];
		else
			__name = "anonymous";
		if ( debug !== undefined && debug )
			console.time( __name );
		if ( debug !== undefined && debug )
			console.groupCollapsed( __name );
		var __result;
		try {
			var args = getArgsArray( arguments );
			var __scope = args.shift();
			if ( this[ "apply" ] !== undefined ) {
				if ( debug )
					console.log( "Executing " + __name + "\nwith arguments: " + args );
				var toString = Object.prototype.toString;
				if ( toString.call( args[ 0 ] ) === "[object Array]" )
					__result = this.apply( __scope, args[ 0 ] ); // call original method and return
				else
					__result = this.apply( __scope, args ); // call original method and return
			}
		} catch ( err ) {
			// if ( debug ) {
			// 	var trace = printStackTrace( {
			// 		e: err
			// 	} );
			// 	console.warn( __name + " error:\n" + err.message + "\nStack trace:\n" + trace.join( "\n" ) );
			// }
		}
		if ( debug !== undefined && debug )
			console.groupEnd( __name );
		if ( debug !== undefined && debug )
			console.timeEnd( __name );
		return __result;
	};
} )();
// }
