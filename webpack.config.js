const globby = require( 'globby' )
const path = require( 'path' )
const webpack = require( "webpack" );
// const jQuery = require( "jquery" );
// const mockjax = require ( 'jquery-mockjax' );
const OptimizeJsPlugin = require("optimize-js-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');

const files = globby.sync( [ './src/js/**/*.js','./src/js/external/**/*.js','./src/js/lib/WebBlocks/**/*.js', '!./src/js/lib/*' ] )
const entry = Object.assign( ...files.map( ( f ) => {
	const i = f.split( '/' )
		.pop()
		.slice( 0, -3 )
	return {
    [ i ]: f,
	}
} ) )

module.exports = {
	entry,
	output: {
		path: path.resolve( './build/js' ),
		filename: '[name].js',
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {
					presets: [ 'es2015' ],
				},
      },
    ],
	},
	plugins: [
   // new webpack.ProvidePlugin( {
				// $: "jquery",
				// jQuery: "jquery"
			// } ),
			// new webpack.optimize.UglifyJsPlugin(options),
   // new OptimizeJsPlugin({
   	// sourceMap: false
   // })
	 	new CopyWebpackPlugin([
		   // Copy directory contents to {output}/
		   { from: 'src/js/external', to: 'external' }
				])
	]
}
